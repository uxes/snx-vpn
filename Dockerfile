FROM ubuntu

COPY snx_install.sh /opt/snx_install.sh
RUN dpkg --add-architecture i386 && apt-get update
RUN apt-get update
RUN apt-get install -y kmod libnss3-tools libpam0g:i386 libstdc++5 libx11-6:i386 libstdc++6:i386 libstdc++5:i386
RUN chmod a+rx /opt/snx_install.sh && /opt/snx_install.sh
