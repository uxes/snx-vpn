# snx installer taken from
```
wget https://starkers.keybase.pub/snx_install_linux30.sh?dl=1 -O snx_install.sh
```

# Usage
```
docker build -t snx .

docker run -it --cap-add=ALL -v /lib/modules:/lib/modules -v $(pwd):/cc snx bash

snx -g -s vpn.somecompany.com -c /cc/my_token.p12

```